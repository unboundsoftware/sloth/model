/*
 * MIT License
 *
 * Copyright (c) 2023 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package shared

import (
	"net"
	"net/rpc"

	"github.com/hashicorp/go-plugin"

	"gitlab.com/unboundsoftware/sloth/model"
)

type StoreStartArgs struct {
	HandlerServer uint32
	QuitterServer uint32
}

// StoreServerRPC is used by plugins to map RPC calls from the clients to
// methods of the model.Store interface.
type StoreServerRPC struct {
	Broker      *plugin.MuxBroker
	Impl        model.Store
	handlerConn net.Conn
	quitterConn net.Conn
}

func (s *StoreServerRPC) Configure(args []string, reply *ConfigureReply) error {
	configured, err := s.Impl.Configure(args)
	reply.Configured = configured
	reply.Err = err
	return nil
}

func (s *StoreServerRPC) Start(args StoreStartArgs, reply *error) error {
	handlerConn, err := s.Broker.Dial(args.HandlerServer)
	if err != nil {
		return err
	}
	s.handlerConn = handlerConn
	handlerClient := rpc.NewClient(handlerConn)
	handler := &HandlerClientRPC{client: handlerClient}

	quitterConn, err := s.Broker.Dial(args.QuitterServer)
	if err != nil {
		return err
	}
	s.quitterConn = quitterConn
	quitterClient := rpc.NewClient(quitterConn)
	quitter := &QuitterClientRPC{client: quitterClient}

	*reply = s.Impl.Start(handler, quitter)
	return nil
}

func (s *StoreServerRPC) Stop(_ struct{}, reply *error) error {
	defer func() {
		_ = s.handlerConn.Close()
	}()
	defer func() {
		_ = s.quitterConn.Close()
	}()
	*reply = s.Impl.Stop()
	return nil
}

func (s *StoreServerRPC) Add(request model.Request, reply *error) error {
	*reply = s.Impl.Add(request)
	return nil
}

// StoreClientRPC is used by clients (main application) to translate the
// model.Store interface of plugins to RPC calls.
type StoreClientRPC struct {
	Broker *plugin.MuxBroker
	Client *rpc.Client
}

func (s *StoreClientRPC) IsSource() bool {
	return false
}

func (s *StoreClientRPC) IsSink() bool {
	return false
}

func (s *StoreClientRPC) IsStore() bool {
	return true
}

func (s *StoreClientRPC) Configure(args []string) (bool, error) {
	var resp ConfigureReply
	err := s.Client.Call("Plugin.Configure", args, &resp)
	if err != nil {
		return false, err
	}
	return resp.Configured, resp.Err
}

func (s *StoreClientRPC) Start(handler model.Handler, quitter model.Quitter) error {
	handlerServerID := s.Broker.NextId()
	handlerServer := &HandlerServerRPC{Impl: handler}
	go s.Broker.AcceptAndServe(handlerServerID, handlerServer)

	quitterServerID := s.Broker.NextId()
	quitterServer := &QuitterServerRPC{Impl: quitter}
	go s.Broker.AcceptAndServe(quitterServerID, quitterServer)

	var resp error
	err := s.Client.Call("Plugin.Start", StoreStartArgs{
		HandlerServer: handlerServerID,
		QuitterServer: quitterServerID,
	}, &resp)
	if err != nil {
		return err
	}

	return resp
}

func (s *StoreClientRPC) Stop() error {
	var resp error
	err := s.Client.Call("Plugin.Stop", struct{}{}, &resp)
	if err != nil {
		return err
	}

	return resp
}

func (s *StoreClientRPC) Add(request model.Request) error {
	var resp error
	err := s.Client.Call("Plugin.Add", request, &resp)
	if err != nil {
		return err
	}

	return resp
}

var _ model.Store = &StoreClientRPC{}

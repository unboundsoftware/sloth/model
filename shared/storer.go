/*
 * MIT License
 *
 * Copyright (c) 2023 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package shared

import (
	"net/rpc"

	"gitlab.com/unboundsoftware/sloth/model"
)

type StorerStoreArgs struct {
	Request model.Request
}

// StorerServerRPC is used by plugins to map RPC calls from the clients to
// methods of the model.Storer interface.
type StorerServerRPC struct {
	Impl model.Storer
}

func (s *StorerServerRPC) Store(request StorerStoreArgs, reply *error) error {
	*reply = s.Impl.Store(request.Request)
	return nil
}

// StorerClientRPC is used by clients (plugins) to translate the
// model.Storer interface of plugins to RPC calls.
type StorerClientRPC struct {
	client *rpc.Client
}

func (s StorerClientRPC) Store(request model.Request) error {
	var resp error
	err := s.client.Call("Plugin.Store", StorerStoreArgs{Request: request}, &resp)
	if err != nil {
		return err
	}
	return resp
}

var _ model.Storer = &StorerClientRPC{}

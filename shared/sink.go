/*
 * MIT License
 *
 * Copyright (c) 2023 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package shared

import (
	"log/slog"
	"net"
	"net/rpc"

	"github.com/hashicorp/go-plugin"

	"gitlab.com/unboundsoftware/sloth/model"
)

type SinkStartArgs struct {
	QuitterServer uint32
}

// SinkServerRPC is used by plugins to map RPC calls from the clients to
// methods of the model.Sink interface.
type SinkServerRPC struct {
	Broker *plugin.MuxBroker
	Impl   model.Sink
	conn   net.Conn
}

type ConfigureReply struct {
	Configured bool
	Err        error
}

func (s *SinkServerRPC) Configure(args []string, reply *ConfigureReply) error {
	configured, err := s.Impl.Configure(args)
	reply.Configured = configured
	reply.Err = err
	return nil
}

func (s *SinkServerRPC) Start(args SinkStartArgs, reply *error) error {
	conn, err := s.Broker.Dial(args.QuitterServer)
	if err != nil {
		return err
	}
	s.conn = conn

	client := rpc.NewClient(conn)
	quitter := &QuitterClientRPC{client: client}
	*reply = s.Impl.Start(quitter)
	return nil
}

func (s *SinkServerRPC) Stop(_ struct{}, reply *error) error {
	defer func() {
		_ = s.conn.Close()
	}()
	*reply = s.Impl.Stop()
	return nil
}

func (s *SinkServerRPC) Publish(request model.Request, reply *error) error {
	*reply = s.Impl.Publish(request)
	return nil
}
func (s *SinkServerRPC) Accept(scheme string, resp *bool) error {
	*resp = s.Impl.Accept(scheme)
	return nil
}

// SinkClientRPC is used by clients (main application) to translate the
// model.Sink interface of plugins to RPC calls.
type SinkClientRPC struct {
	Broker *plugin.MuxBroker
	Client *rpc.Client
}

func (s *SinkClientRPC) IsSource() bool {
	return false
}

func (s *SinkClientRPC) IsSink() bool {
	return true
}

func (s *SinkClientRPC) IsStore() bool {
	return false
}

func (s *SinkClientRPC) Configure(args []string) (bool, error) {
	var resp ConfigureReply
	err := s.Client.Call("Plugin.Configure", args, &resp)
	if err != nil {
		return false, err
	}
	return resp.Configured, resp.Err
}

func (s *SinkClientRPC) Start(quitter model.Quitter) error {
	quitterServerID := s.Broker.NextId()
	quitterServer := &QuitterServerRPC{Impl: quitter}
	go s.Broker.AcceptAndServe(quitterServerID, quitterServer)

	var resp error
	err := s.Client.Call("Plugin.Start", SinkStartArgs{
		QuitterServer: quitterServerID,
	}, &resp)
	if err != nil {
		return err
	}

	return resp
}

func (s *SinkClientRPC) Publish(request model.Request) error {
	var resp error
	err := s.Client.Call("Plugin.Publish", request, &resp)
	if err != nil {
		return err
	}
	return resp
}

func (s *SinkClientRPC) Stop() error {
	var resp error
	err := s.Client.Call("Plugin.Stop", struct{}{}, &resp)
	if err != nil {
		return err
	}
	return resp
}

func (s *SinkClientRPC) Accept(scheme string) bool {
	var resp bool
	err := s.Client.Call("Plugin.Accept", scheme, &resp)
	if err != nil {
		slog.Error("accept", "error", err)
		return false
	}
	return resp
}

var _ model.Sink = &SinkClientRPC{}

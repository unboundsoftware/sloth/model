/*
 * MIT License
 *
 * Copyright (c) 2023 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package shared

import (
	"net"
	"net/rpc"

	"github.com/hashicorp/go-plugin"

	"gitlab.com/unboundsoftware/sloth/model"
)

// SourceServerRPC is used by plugins to map RPC calls from the clients to
// methods of the model.Source interface.
type SourceServerRPC struct {
	Broker      *plugin.MuxBroker
	Impl        model.Source
	storerConn  net.Conn
	quitterConn net.Conn
}

type SourceStartArgs struct {
	StorerServer  uint32
	QuitterServer uint32
}

func (s *SourceServerRPC) Configure(args []string, reply *ConfigureReply) error {
	configured, err := s.Impl.Configure(args)
	reply.Configured = configured
	reply.Err = err
	return nil
}

func (s *SourceServerRPC) Start(args SourceStartArgs, reply *error) error {
	storerConn, err := s.Broker.Dial(args.StorerServer)
	if err != nil {
		return err
	}
	s.storerConn = storerConn
	storerClient := rpc.NewClient(storerConn)
	storer := &StorerClientRPC{client: storerClient}

	quitterConn, err := s.Broker.Dial(args.QuitterServer)
	if err != nil {
		return err
	}
	s.quitterConn = quitterConn
	quitterClient := rpc.NewClient(quitterConn)
	quitter := &QuitterClientRPC{client: quitterClient}
	*reply = s.Impl.Start(storer, quitter)
	return nil
}

func (s *SourceServerRPC) Stop(_ struct{}, reply *error) error {
	defer func() {
		_ = s.storerConn.Close()
	}()
	defer func() {
		_ = s.quitterConn.Close()
	}()
	*reply = s.Impl.Stop()
	return nil
}

// SourceClientRPC is used by clients (main application) to translate the
// model.Source interface of plugins to RPC calls.
type SourceClientRPC struct {
	Broker *plugin.MuxBroker
	Client *rpc.Client
}

func (s *SourceClientRPC) IsSource() bool {
	return true
}

func (s *SourceClientRPC) IsSink() bool {
	return false
}

func (s *SourceClientRPC) IsStore() bool {
	return false
}

func (s *SourceClientRPC) Configure(args []string) (bool, error) {
	var resp ConfigureReply
	err := s.Client.Call("Plugin.Configure", args, &resp)
	if err != nil {
		return false, err
	}
	return resp.Configured, resp.Err
}

func (s *SourceClientRPC) Start(storer model.Storer, quitter model.Quitter) error {
	storerServerID := s.Broker.NextId()
	storerServer := &StorerServerRPC{Impl: storer}
	go s.Broker.AcceptAndServe(storerServerID, storerServer)

	quitterServerID := s.Broker.NextId()
	quitterServer := &QuitterServerRPC{Impl: quitter}
	go s.Broker.AcceptAndServe(quitterServerID, quitterServer)

	var resp error
	err := s.Client.Call("Plugin.Start", &SourceStartArgs{
		StorerServer:  storerServerID,
		QuitterServer: quitterServerID,
	}, &resp)
	if err != nil {
		return err
	}

	return resp
}

func (s *SourceClientRPC) Stop() error {
	var resp error
	err := s.Client.Call("Plugin.Stop", struct{}{}, &resp)
	if err != nil {
		return err
	}
	return resp
}

var _ model.Source = &SourceClientRPC{}

/*
 * MIT License
 *
 * Copyright (c) 2021 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package model

import (
	"encoding/json"
	"time"
)

// Quitter is used by plugins to signal that the plugin host should terminate.
type Quitter interface {
	Quit(err error)
}

type Request struct {
	DelayedUntil time.Time
	Target       string
	Payload      json.RawMessage
}

// Storer is used to store a request from a model.Source.
type Storer interface {
	Store(request Request) error
}

// Handler is used to handle a request in a model.Store.
type Handler interface {
	Handle(request Request) error
}

// ChannelQuitter is an implementation of Quitter based on an error channel.
type ChannelQuitter chan error

func (q ChannelQuitter) Quit(err error) {
	q <- err
}

// The HandlerFunc type is an adapter to allow the use of
// an ordinary function as a Handler. If f is a function
// with the appropriate signature, HandlerFunc(f) is a
// Handler that calls f.
type HandlerFunc func(request Request) error

func (f HandlerFunc) Handle(request Request) error {
	return f(request)
}

type Plugin interface {
	IsSource() bool
	IsSink() bool
	IsStore() bool
	Configure(args []string) (bool, error)
}

type Source interface {
	Plugin
	Start(storer Storer, quitter Quitter) error
	Stop() error
}

type Sink interface {
	Plugin
	Start(quitter Quitter) error
	Publish(request Request) error
	Stop() error
	Accept(scheme string) bool
}

type Store interface {
	Plugin
	Start(handler Handler, quitter Quitter) error
	Stop() error
	Add(request Request) error
}

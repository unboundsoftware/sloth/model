/*
 * MIT License
 *
 * Copyright (c) 2023 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package plugin

import (
	"net/rpc"

	"github.com/hashicorp/go-plugin"

	"gitlab.com/unboundsoftware/sloth/model"
	"gitlab.com/unboundsoftware/sloth/model/shared"
)

// SourcePlugin implements the plugin.Plugin interface to provide the RPC
// server or client back to the plugin machinery. The server side should
// provide the Impl field with a concrete implementation of the model.Source
// interface.
type SourcePlugin struct {
	Impl model.Source
}

func (p *SourcePlugin) Server(broker *plugin.MuxBroker) (interface{}, error) {
	return &shared.SourceServerRPC{
		Broker: broker,
		Impl:   p.Impl,
	}, nil
}

func (p *SourcePlugin) Client(broker *plugin.MuxBroker, client *rpc.Client) (interface{}, error) {
	return &shared.SourceClientRPC{
		Broker: broker,
		Client: client,
	}, nil
}

// SinkPlugin implements the plugin.Plugin interface to provide the RPC
// server or client back to the plugin machinery. The server side should
// provide the Impl field with a concrete implementation of the model.Sink
// interface.
type SinkPlugin struct {
	Impl model.Sink
}

func (p *SinkPlugin) Server(broker *plugin.MuxBroker) (interface{}, error) {
	return &shared.SinkServerRPC{
		Broker: broker,
		Impl:   p.Impl,
	}, nil
}

func (p *SinkPlugin) Client(broker *plugin.MuxBroker, client *rpc.Client) (interface{}, error) {
	return &shared.SinkClientRPC{
		Broker: broker,
		Client: client,
	}, nil
}

// StorePlugin implements the plugin.Plugin interface to provide the RPC
// server or client back to the plugin machinery. The server side should
// provide the Impl field with a concrete implementation of the model.Store
// interface.
type StorePlugin struct {
	Impl model.Store
}

func (p *StorePlugin) Server(broker *plugin.MuxBroker) (interface{}, error) {
	return &shared.StoreServerRPC{
		Broker: broker,
		Impl:   p.Impl,
	}, nil
}

func (p *StorePlugin) Client(broker *plugin.MuxBroker, client *rpc.Client) (interface{}, error) {
	return &shared.StoreClientRPC{
		Broker: broker,
		Client: client,
	}, nil
}
